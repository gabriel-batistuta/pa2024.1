from abc import ABC, abstractmethod

class TextFormat(ABC):

    @abstractmethod
    def format(self, text) -> str:
        pass

class FormatCenter(TextFormat):
    def format(self, text):
        return f'|\t {text}\t\t(Center)'

class FormatRight(TextFormat):
    def format(self, text):
        return f'|\t\t {text}\t(Right)'
    
class FormatLeft(TextFormat):
    def format(self, text):
        return f'| {text} \t\t\t(Left)'
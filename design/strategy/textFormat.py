import textFormats as tf

class Sistema():
    def __init__(self) -> None:
        self.text_formats = []
    
    def add_text_format(self, text_format):
        self.text_formats.append(text_format)

    def runTextFormat(self, text, text_format):
        print(text_format.format(text))
    
if __name__ == '__main__':
    sistema = Sistema()
    sistema.add_text_format(tf.FormatCenter())
    sistema.add_text_format(tf.FormatRight())
    sistema.add_text_format(tf.FormatLeft())

    for text_format in sistema.text_formats:
        sistema.runTextFormat('test', text_format)
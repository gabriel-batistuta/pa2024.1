package strategy;

public abstract class TextFormat {

    public abstract String format(String text);
}
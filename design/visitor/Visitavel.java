package visitor;

public interface Visitavel {
    public float accept(Visitor v);
}

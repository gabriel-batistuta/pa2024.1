package visitor;

public class Trapezio extends Forma {

    public int baseMaior;
    public int baseMenor;
    public int altura;

    
    public Trapezio(int baseMaior, int baseMenor, int altura) {
        this.altura = altura;
        this.baseMaior = baseMaior;
        this.baseMenor = baseMenor;
    }
    @Override
    public float accept(Visitor visitor) {

        return visitor.visite(this);
    }
    
}

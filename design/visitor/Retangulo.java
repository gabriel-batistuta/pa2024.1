package visitor;

public class Retangulo extends Forma {

    public int lado;
    public int altura;

    public Retangulo(int lado, int altura) {
        this.lado = lado;
        this.altura = altura;
    }
    @Override
    public float accept(Visitor visitor) {
        
        return visitor.visite(this);
    }
    
    
}

class Sistema_Aula:
    __instancia = None

    def __init__(self) -> None:
        self.name = "Sistema Aula"

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name
    
    @classmethod
    def get_instancia(cls):
        if not cls.__instancia:
            cls.__instancia = cls()
        return cls.__instancia

if __name__ == "__main__":
    s1 = Sistema_Aula.get_instancia()
    s2 = Sistema_Aula.get_instancia()

    print(s1 is s2)

    s1.set_name("Qualquer")
    print(s2.get_name())
package singleton;

public class SistemaAula {
    private static SistemaAula sa;

    private String name = "Sistema Aula";
    private SistemaAula() {

    }

    public static SistemaAula getInstancia() {
        if(sa == null) {
            sa = new SistemaAula();
        }

        return sa;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
    public static void main(String[] args) {
        
        SistemaAula s = new SistemaAula();
        s.setName("Qualquer");
        s.getName();
    }
}


package composite;

public class Main2 {
    public static void main(String[] args) {
        
        PlacaMae placaMae = new PlacaMae();  

        placaMae.fabricante = "Fabricante Placa Mãe";

        PlacaVideo placaVideo = new PlacaVideo()
        placaVideo.fabricante = "Fabricante Placa Video";

        Processador processador =  new Processador()
        processador.clock = 2.0;

        CircuitoIntegrado circuitoIntegrado = new CircuitoIntegrado();


        placaMae.addComponente(placaVideo);

        placaMae.addComponente(processador);


        placaVideo.addComponente(circuitoIntegrado);

    }
}

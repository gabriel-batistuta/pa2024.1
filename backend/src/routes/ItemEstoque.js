const router = require('express').Router();
const ObjectControl = require('../model/ObjectControl');

const oc = new ObjectControl();
const _class = "ItemEstoque";

router.post("/",async (req,res)=>{

    let doc = await oc.getDocByForeignKey(_class,{"Produto:_key":"1","Estoque:_key":"1"})

    if(!!doc) {

        doc.quantidade += req.body.quantidade

    } else {

        doc = req.body;
    }
        
    doc = await oc.save(_class,doc)
    
    res.send(doc)
})


router.put("/",async (req,res)=>{

    const docB = req.body

    const doc = await oc.save(_class,docB)
    
    res.send(doc)
})

router.get("/",async (req,res)=>{

    const list = await oc.getListDoc(_class)
    
    res.send(list)
})

router.put("/get",async (req,res)=>{


    const doc = await oc.getDocByKey(_class,req.body._key)
    
    res.send(doc)
})


router.get("/:_key",async (req,res)=>{


    const doc = await oc.getDocByKey(_class,req.params._key)
    
    res.send(doc)
})

router.put("/reset",async (req,res)=>{

    await oc.reset([_class]);

})

router.get("/key/:_key/level/:level",async (req,res)=>{


    const doc = await oc.getDocByKey(_class,req.params._key,req.params.level)
    
    res.send(doc)
})

module.exports = router
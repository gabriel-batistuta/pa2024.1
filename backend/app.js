require('dotenv').config()
const express = require('express')
const cors = require('cors');

const produto = require('./src/routes/Produto')
const person = require('./src/routes/Person')
const user = require('./src/routes/User')
const estoque = require('./src/routes/Estoque')
const itemEstoque = require('./src/routes/ItemEstoque')
const app = express()

app.use(cors({}));

app.use(express.json());

app.use('/itemEstoque',itemEstoque)
app.use('/estoque',estoque)
app.use('/produto',produto)
app.use('/person',person)
app.use('/user',user)

const port = process.env.SERVER_PORT || 3000; 

app.listen(port, () => {
  console.log(`Servidor em execução na porta ${port}`);
});
